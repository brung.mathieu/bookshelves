import { Component, OnInit } from '@angular/core';
import {Book} from "../../models/Book.model";
import {ActivatedRoute, Router} from "@angular/router";
import {BooksService} from "../../services/books.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-single-book',
  templateUrl: './single-book.component.html',
  styleUrls: ['./single-book.component.scss']
})
export class SingleBookComponent implements OnInit {

  DEFAULT_BOOK_IMG_DL:string = 'https://firebasestorage.googleapis.com/v0/b/bookshelves-ab810.appspot.com/o/images%2Fdefault-book.png?alt=media&token=7fd9b39a-c80a-425a-9f3a-949715873b4d';

  //@ts-ignore
  book: Book;

  //@ts-ignore
  updateBookForm: FormGroup;
  modifyMode: boolean = false;
  fileIsUploading: boolean = false;
  fileUrl?: string;
  fileIsUploaded: boolean = false;

  constructor(private route: ActivatedRoute,
              private booksService: BooksService,
              private router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.book = new Book('', '');
    const id = this.route.snapshot.params['id'];
    this.booksService.getSingleBook(+id)
      .then(
    // @ts-ignore
        (book: Book) => {
          this.book = book;
        }
      )
  }

  onBack() {
    this.router.navigate(['/books']);
  }

  onModify() {
    this.modifyMode = true;
    this.initForm();
  }

  initForm() {
    this.updateBookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      synopsis: ''
    });
  }

  onUpdateBook() {
    const title = this.updateBookForm.get('title')?.value;
    const author = this.updateBookForm.get('author')?.value;
    const synopsis = this.updateBookForm.get('synopsis')?.value;
    const id = this.route.snapshot.params['id'];
    let photo = this.book.photo;

    if (this.fileUrl && this.fileUrl !== '') {
      photo = this.fileUrl;
    } else if (this.book.photo === ''){
      photo = this.DEFAULT_BOOK_IMG_DL;
    }

    this.booksService.updateBook(+id, title, author, synopsis, photo);
    this.router.navigate(['/books'], id);
  }

  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.booksService.uploadFile(file).then(
      // @ts-ignore
      (url: string) => {
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileIsUploaded = true;
      }
    );
  }

  detectFiles(event: any) {
    this.onUploadFile(event.target.files[0]);
  }
}
