import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {BooksService} from "../../services/books.service";
import {Book} from "../../models/Book.model";
import {DEFAULTS} from "@angular/fire/compat/remote-config";

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {

  DEFAULT_BOOK_IMG_DL:string = 'https://firebasestorage.googleapis.com/v0/b/bookshelves-ab810.appspot.com/o/images%2Fdefault-book.png?alt=media&token=7fd9b39a-c80a-425a-9f3a-949715873b4d';

  //@ts-ignore
  bookForm: FormGroup;

  fileIsUploading: boolean = false;
  fileUrl?: string;
  fileIsUploaded: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private booksService: BooksService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      synopsis: ''
    });
  }

  onSaveBook() {
    const title = this.bookForm.get('title')?.value;
    const author = this.bookForm.get('author')?.value;
    const synopsis = this.bookForm.get('synopsis')?.value;
    const newBook = new Book(title, author);
    newBook.synopsis = synopsis;
    if (this.fileUrl && this.fileUrl !== '') {
      newBook.photo = this.fileUrl;
    } else {
      newBook.photo = this.DEFAULT_BOOK_IMG_DL;
    }
    this.booksService.createNewBook(newBook);
    this.router.navigate(['/books']);
  }

  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.booksService.uploadFile(file).then(
    // @ts-ignore
      (url: string) => {
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileIsUploaded = true;
      }
    );
  }

  detectFiles(event: any) {
    this.onUploadFile(event.target.files[0]);
  }

  onBack() {
    this.router.navigate(['/books']);
  }

}
