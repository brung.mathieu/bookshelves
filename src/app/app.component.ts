import { Component } from '@angular/core';
import firebase from "firebase/compat/app";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title:string = "Ma bibliothèque";

  constructor() {

    const config = {
      apiKey: "AIzaSyCxNkD4hUOhVEO6asy4r5wa2qUkj7nw2kk",
      authDomain: "bookshelves-ab810.firebaseapp.com",
      projectId: "bookshelves-ab810",
      databaseURL: "https://bookshelves-ab810-default-rtdb.europe-west1.firebasedatabase.app",
      storageBucket: "bookshelves-ab810.appspot.com",
      messagingSenderId: "782349307966",
      appId: "1:782349307966:web:34821322ea5e377fa2816f"
    };

    firebase.initializeApp(config);
  }

}
