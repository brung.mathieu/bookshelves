import {Component, Input, OnInit} from '@angular/core';
import firebase from "firebase/compat/app";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() appTitle?: string;

  isAuth: boolean = false;
  email?: string | null;

  changeTheme: boolean = false;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          this.isAuth = true;
          this.email = user.email;
        } else {
          this.isAuth = false;
        }
      }
    );
  }

  onSignOut() {
    this.authService.signOutUser();
  }

  onChangeTheme() {
    this.changeTheme = !this.changeTheme;
  }
}
