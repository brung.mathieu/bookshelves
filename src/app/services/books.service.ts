import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {Book} from "../models/Book.model";
import firebase from "firebase/compat/app";
import "firebase/compat/database";
import "firebase/compat/storage";

@Injectable({
  providedIn: 'root'
})

export class BooksService {

  DEFAULT_BOOK_IMG_STORAGE:string = 'gs://bookshelves-ab810.appspot.com/images/default-book.png'

  books: Book[] = [];
  bookSubject = new Subject<Book[]>()

  constructor() { }

  emitBooks() {
    this.bookSubject.next(this.books);
  }

  saveBooks() {
    firebase.database()
      .ref('/books')
      .set(this.books);
  }

  getBooks() {
    firebase.database()
      .ref('/books')
      .on('value', (data) => {
        this.books = data.val() ? data.val() : [];
        this.emitBooks();
      });
  }

  getSingleBook(id: number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/books/' + id)
          .once('value')
          .then(
            (data) => {
              resolve(data.val());
            }, (error) => {
              reject(error);
            }
          );
      }
    );
  }

  createNewBook(newBook: Book) {
    this.books.push(newBook);
    this.saveBooks();
    this.emitBooks();
  }

  getBookById(id: number) {
    return firebase.database().ref('/books/' + id);
  }

  updateBook(id: number, newTitle:string, newAuthor:string, newSynopsis: string, newPhoto: string) {
    const book = this.getBookById(id);
    book.update({
      title: newTitle,
      author: newAuthor,
      synopsis: newSynopsis,
      photo: newPhoto
    });
    console.log('Update effectuée');
  }

  removeBook(book: Book) {
    if (book.photo) {
      const storageRef = firebase.storage().refFromURL(book.photo);
      if (storageRef.toString() !== this.DEFAULT_BOOK_IMG_STORAGE) {
        storageRef.delete().then(
          () => {
            console.log('Photo supprimée.');
          }
        ).catch(
          (error) => {
            console.log('Fichier non trouvé : ' + error);
          }
        );
      }
    }

    const bookIndexToRemove = this.books.findIndex(
    // @ts-ignore
      (bookEl) => {
        if (bookEl === book) {
          return true;
        }
      }
    );
    this.books.splice(bookIndexToRemove, 1);
    this.saveBooks();
    this.emitBooks();
  }

  uploadFile(file: File) {
    return new Promise(
      (resolve, reject) => {
        const almostUniqueFileName = Date.now().toString();
        const upload = firebase.storage().ref()
          .child('images/' + almostUniqueFileName + file.name)
          .put(file);

        upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
          () => {
            console.log('Chargement...');
          },
          (error) => {
            console.log('Erreur de chargement : ' + error);
            reject();
          },
          () => {
            resolve(upload.snapshot.ref.getDownloadURL());
          })
      }
    )
  }

}
