import {Component, OnDestroy, OnInit} from '@angular/core';
import firebase from "firebase/compat/app";
import {interval, Observable, of} from "rxjs";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {

  isAuth: boolean = false;

  secondes: number = 0;
  // @ts-ignore
  counterSubscription: Subscription;

  //Exemple d'utilisation d'Observable
  //Date Async Observable
  dateAsyncObservable: Observable<Date> | undefined;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          this.secondes = 0;
          this.isAuth = true;
          const counter = interval(1000)
          this.counterSubscription = counter.subscribe(
            (value: number) => {
              this.secondes = value;
            }
          )
          this.dateAsyncObservable = this.makeObservable();
        } else {
          this.isAuth = false;
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.secondes = 0;
    this.counterSubscription.unsubscribe();
  }

  makeObservable(): Observable<Date> {
    const date = new Date();
    return of(date);
  }

}
